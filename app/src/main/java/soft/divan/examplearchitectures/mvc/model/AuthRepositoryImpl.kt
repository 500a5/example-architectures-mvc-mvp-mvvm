package soft.divan.examplearchitectures.mvc.model

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async


class AuthRepositoryImpl : AuthRepository {

    override suspend fun login(email: String, password: String): Deferred<String> {
        // Мок задержки и возврат отсутствия ошибки
        return GlobalScope.async {
            Thread.sleep(2000)
            ""
        }
    }
}
