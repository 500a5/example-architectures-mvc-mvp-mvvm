package soft.divan.examplearchitectures.mvc.controller

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import soft.divan.examplearchitectures.databinding.ActivityMvcBinding
import soft.divan.examplearchitectures.mvc.model.AuthRepositoryImpl


class MvcActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMvcBinding
    private val authRepository = AuthRepositoryImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMvcBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {
            if (!validateEmail()) return@setOnClickListener
            if (!validatePassword()) return@setOnClickListener
            login()
        }
    }

    private fun login() {
        CoroutineScope(Dispatchers.IO).async {
            val errorMessage = authRepository.login(
                email = binding.textEmail.text.toString(),
                password = binding.textPassword.text.toString()
            ).await()
            if (errorMessage.isEmpty())
                launch(Dispatchers.Main) {
                    Toast.makeText(this@MvcActivity, "Success login", Toast.LENGTH_LONG).show()
                }
            else launch(Dispatchers.Main) {
                Toast.makeText(this@MvcActivity, errorMessage, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun validateEmail(): Boolean {
        val email = binding.textEmail.text.toString()
        return (email.contains("@") && email.contains("."))
    }

    private fun validatePassword(): Boolean {
        val password = binding.textPassword.text.toString()
        return password.length > 6
    }

}