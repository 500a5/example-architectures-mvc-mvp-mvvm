package soft.divan.examplearchitectures.mvvm.model

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async


class AuthRepositoryImpl : AuthRepository {

    override suspend fun login(email: String, password: String): Deferred<String> {
        // Мок задержки и возвращение ошибки
        return GlobalScope.async {
            Thread.sleep(2000)
            ""
        }
    }
}
