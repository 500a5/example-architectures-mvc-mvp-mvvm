package soft.divan.examplearchitectures.mvvm.view

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import soft.divan.examplearchitectures.databinding.ActivityMvpBinding
import soft.divan.examplearchitectures.mvvm.viewModel.LoginState
import soft.divan.examplearchitectures.mvvm.viewModel.LoginViewModel

class MvvmActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMvpBinding
    private val viewModel by viewModels<LoginViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMvpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.state.observe(
            this@MvvmActivity
        ) { state ->
            when (state) {
                is LoginState.LoginSucceededLoginState -> {
                    Toast.makeText(this, "Success login", Toast.LENGTH_LONG).show()
                }

                is LoginState.SendingLoginState -> {
                    enableUi(false)
                }

                is LoginState.ErrorLoginState -> {
                    Toast.makeText(this, state.message, Toast.LENGTH_LONG).show()
                    enableUi(true)
                }

                is LoginState.DefaultState -> {
                    enableUi(true)
                }

            }
        }

        binding.btnLogin.setOnClickListener {
            viewModel.login(binding.textEmail.text.toString(), binding.textPassword.text.toString())
        }
    }

    private fun enableUi(value: Boolean) {
        binding.textEmail.isEnabled = value
        binding.textPassword.isEnabled = value
        binding.btnLogin.isEnabled = value
    }
}