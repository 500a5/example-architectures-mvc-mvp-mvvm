package soft.divan.examplearchitectures.mvvm.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import soft.divan.examplearchitectures.mvvm.model.AuthRepositoryImpl

sealed class LoginState {
    data object DefaultState : LoginState()
    data object SendingLoginState : LoginState()
    data object LoginSucceededLoginState : LoginState()
    class ErrorLoginState(val message: String) : LoginState()

}

class LoginViewModel : ViewModel() {
    private var authRepository = AuthRepositoryImpl()
    val state = MutableLiveData<LoginState>().apply { value = LoginState.DefaultState }

    fun login(email: String, password: String) {
        if (!validateEmail(email)) return

        if (!validatePassword(password)) return

        CoroutineScope(Dispatchers.IO).async {
            val errorMessage = authRepository.login(email, password).await()
            if (errorMessage.isEmpty())
                launch(Dispatchers.Main) {
                    state.apply { value = LoginState.LoginSucceededLoginState }
                }
            else launch(Dispatchers.Main) {
                state.apply { value = LoginState.ErrorLoginState(errorMessage) }

            }
        }
    }

    private fun validateEmail(email: String): Boolean {
        return (email.contains("@") && email.contains("."))
    }

    private fun validatePassword(password: String): Boolean {
        return password.length > 6
    }
}