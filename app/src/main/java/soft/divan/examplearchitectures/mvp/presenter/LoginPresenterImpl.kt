package soft.divan.examplearchitectures.mvp.presenter

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import soft.divan.examplearchitectures.mvp.model.AuthRepositoryImpl
import java.lang.ref.WeakReference

class LoginPresenterImpl : LoginPresenter {
    private val authRepository = AuthRepositoryImpl()
    private var viewState: WeakReference<LoginView>? = null

    fun attachView(view: LoginView) {
        viewState = WeakReference(view)
    }

    override fun login(email: String, password: String) {
        if (!validateEmail(email)) return
        if (!validatePassword(password)) return
        CoroutineScope(Dispatchers.IO).async {
            val errorMessage = authRepository.login(email, password).await()
            if (errorMessage.isEmpty())
                launch(Dispatchers.Main) {
                    viewState?.get()?.showSuccess()
                }
            else launch(Dispatchers.Main) {
                viewState?.get()?.showError(errorMessage)
            }
        }
    }

    private fun validateEmail(email: String): Boolean {
        return (email.contains("@") && email.contains("."))
    }

    private fun validatePassword(password: String): Boolean {
        return password.length > 6
    }
}