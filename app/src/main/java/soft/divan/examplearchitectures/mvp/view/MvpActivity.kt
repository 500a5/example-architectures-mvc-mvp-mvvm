package soft.divan.examplearchitectures.mvp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import soft.divan.examplearchitectures.databinding.ActivityMvpBinding
import soft.divan.examplearchitectures.mvp.presenter.LoginPresenterImpl
import soft.divan.examplearchitectures.mvp.presenter.LoginView

class MvpActivity : AppCompatActivity(), LoginView {
    private lateinit var binding: ActivityMvpBinding
    private val loginPresenter = LoginPresenterImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMvpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        loginPresenter.attachView(this)
        binding.btnLogin.setOnClickListener {
            loginPresenter.login(binding.textEmail.text.toString(), binding.textPassword.text.toString())
        }
    }

    override fun showSuccess() {
        Toast.makeText(this@MvpActivity, "Success login", Toast.LENGTH_LONG).show()
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(this@MvpActivity, errorMessage, Toast.LENGTH_LONG).show()
    }

}
