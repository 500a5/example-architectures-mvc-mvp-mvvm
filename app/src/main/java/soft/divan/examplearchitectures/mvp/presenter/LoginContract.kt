package soft.divan.examplearchitectures.mvp.presenter

interface LoginPresenter {
    fun login(email: String, password: String)
}

interface LoginView {
    fun showSuccess()
    fun showError(errorMessage: String)
}